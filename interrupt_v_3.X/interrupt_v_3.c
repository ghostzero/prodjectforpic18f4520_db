/*
 * File:   interrupt_v_3.c
 * Author: Ghost
 *
 * Created on 27 июня 2016 г., 13:29
 */


//#include <p18F4520.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <math.h>
//#include <float.h>
//#include <delays.h>

#include <xc.h>

void init(void); // Начальная инициализация микроконтроллера
void Interrupt_Isr(void);
int flag_view = 1;
int PRESSED_BUTTON = 0, DEPRESSED_BUTTON = 1;
int LOW = 0, HIGH = 1;
int tick_count=0;

void init(void) { // Начальная инициализация микроконтроллера
    T1CON = 0x01;               //Configure Timer1 interrupt
    PIE1bits.TMR1IE = 1;           
    INTCONbits.PEIE = 1;
    RCONbits.IPEN=0x01;
    IPR1bits.TMR1IP=0x01;            // TMR1 high priority ,TMR1 Overflow Interrupt Priority bit
    INTCONbits.GIE = 1;
    PIR1bits.TMR1IF = 0;
    T0CON=0X00;
    INTCONbits.T0IE = 1;               // Enable interrupt on TMR0 overflow
    INTCON2bits.TMR0IP=0x00;        
    T0CONbits.TMR0ON = 1;
    TRISA = 0b00010000; // RA4 is for Switch SW1 Input
    TRISB = 0b00000001; // RB0 is for Switch SW2 Input
    TRISC = 0b00000000;
    TRISD = 0b00000000;
    TRISE = 0b00001000; // RE3 is for Switch SW3 Input
    PORTB = 0b00001011;
//    ADCON1 = 0x0F;

}


//----------------------------------------------------   MAIN   --------------------------------

void main(void) {
    init();

    //    PORTB = 4;
    while (1) {
//                if(PORTBbits.RB0 ==PRESSED_BUTTON ) PORTB = 0b00001101; else PORTB = 0b00001011;
        //            PORTBbits.RB2 = 1;
        /*if (PORTBbits.RB0 == 0) {
//            PORTBbits.RB1 = 0;
            PORTB = 0b00001100;
         } else {
//PORTBbits.RB2 = 0;
            PORTB = 0b00001010;
        }*/
        if (flag_view == 1) PORTB = 0b00001101;
        if (flag_view == 2) PORTB = 0b00001011;
    } // end of main

    //------------------------------High Priority Interrupts Vector-----------------------------------
}

void interrupt tc_int(void)             // High priority interrupt
{
    if (TMR1IE && TMR1IF)
    {
         if (flag_view == 1) flag_view=2; else flag_view=1;
        TMR1IF=0;
        ++tick_count;
        TRISC=1;
        LATCbits.LATC4 = 0x01;
    }
}


void interrupt low_priority   LowIsr(void)    //Low priority interrupt
{
    if(INTCONbits.T0IF && INTCONbits.T0IE)  // If Timer flag is set & Interrupt is enabled
    {                               
        TMR0 -= 250;                    // Reload the timer - 250uS per interrupt
        INTCONbits.T0IF = 0;            // Clear the interrupt flag 
        ADCON1=0x0F;
        TRISB=0x0CF;
        LATBbits.LATB5 = 0x01;          // Toggle a bit 
    }
    if (TMR1IE && TMR1IF)
    {
        TMR1IF=0;
        ++tick_count;
        TRISC=0;
        LATCbits.LATC3 = 0x01;
    }
}



