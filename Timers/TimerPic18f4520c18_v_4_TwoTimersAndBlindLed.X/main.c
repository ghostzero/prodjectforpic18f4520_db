/*
 * File:   main.c
 * Author: Ghost
 *
 * Created on 6 августа 2016 г., 12:07
 */

//Пример 6.18

/* Установить биты конфигурации 
 * - Установить генератор
 * - Отключить сторожевой таймер
 * - Отключить низковальтное программирование
 * - Отключить сброс по частичной потере питания
 * - Разрешить общий сброс
 *
 *
 */

//#include <p18f4520.h>
//#include <p18f4520.h>

#include "configure.h"
#include <delays.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
#define LED0 PORTDbits.RD0
#define LED1 PORTDbits.RD1
#define LED2 PORTDbits.RD2
#define LED3 PORTDbits.RD3
#define LED4 PORTDbits.RD4
#define LED5 PORTDbits.RD5
#define LED6 PORTDbits.RD6
#define LED7 PORTDbits.RD7
#define LEDbuf0 PORTBbits.RB4
#define LEDbuf1 PORTBbits.RB5
#define LEDbuf2 PORTBbits.RB6
#define LEDbuf3 PORTBbits.RB7
#define delayForButtonSwitcher 15;
#define waitFullButtonPress 100;


int k = 4, i = 1, time_step_1 = 1, time_step_3 = 1;
int PRESSED_BUTTON = 0, DEPRESSED_BUTTON = 1;
int flag_ledRB1 = 0, flag_view_steps_interval_beam = 0, flag_view_steps_duration_beam = 0;

volatile unsigned int steps_duration_beam, steps_interval_beam;








/************прототипы функций обработки прерываний ***************/
void init(void);
void Set_segment_display_v_3(int Number_Led_Buffer, int Data_Byte);
void set_number(int Data_Number);
void MyHighInt();
void DoTime();
void view_all_number_by_7_segments_by_4_elements(int current_number);
void Set_segment_display_v_4(int Number_Led_Buffer, int Data_Byte);
// for switch different button
void switch_button(char current_port, char bit_value);
void switch_button_for_port_e(char bit_value);


#pragma interrupt MyHighInt
#pragma code high_vector = 0x08

/************ вектор высокоприоритетного прерывания ***************/

void high_vector(void) {
    _asm GOTO MyHighInt _endasm;
}

/************ память данных ***************/

// счетчики времени дня

#pragma udata access IntMyData  // размещение в банке доступа

near char seconds;
near char minutes;
near char hours;

// ************************** функций ***********************************

#pragma code

//функций инкрементирования таймера

void DoTime() { //формирование сигнала счета каждую секунду
    //    TMR1H = 0x8000;
    //    TMR1L = 0xFF;

    //        TMR1L = 0x10;
    //    PIR1bits.TMR1IF = 0;

    seconds++;
    if (seconds == 60) {
        seconds = 0;
        minutes++;
        if (minutes == 60) {
            minutes = 0;
            hours++;
            if (hours == 24) {
                hours = 0;
            }
        }

    }

}

void delaysecond_user(int number_seconds) {
    int i;
    for (i = 0; i < number_seconds; i++) {
        //        Delay10KTCYx(25);
        // This configuration for OSC=HS crystale 20MHz
        Delay10KTCYx(500);
    }
}

void delaymillisecond_user(int number_milliseconds) {
    int i;
    for (i = 0; i < number_milliseconds; i++) {
        //        Delay10KTCYx(25);
        // This configuration for OSC=HS crystale 20MHz
        Delay1KTCYx(5);
    }
}

//Процедура обслуживания прерывания

void MyHighInt(void) {
    int test;

    if (PIR1bits.TMR1IF == 1) {
        TMR1L = 0x50;
        TMR1H = 0xc3;
        PIR1bits.TMR1IF = 0;
        if (time_step_1 ==  steps_interval_beam) {
            time_step_1 = 0;
            //            DoTime();
            T3CONbits.TMR3ON = 1;
            T1CONbits.TMR1ON = 0;
            PORTBbits.RB1 = 1;
        }
        time_step_1++;
    }

    if (PIR2bits.TMR3IF == 1) {
        TMR3L = 0x50;
        TMR3H = 0xc3;
        PIR2bits.TMR3IF = 0;
        if (time_step_3 ==steps_duration_beam) {
            time_step_3 = 0;
            T3CONbits.TMR3ON = 0;
            T1CONbits.TMR1ON = 1;
            PORTBbits.RB1 = 0;
        }
        time_step_3++;
    }
}

void main(void) {

    int status_button_re_0 = DEPRESSED_BUTTON;
    int count_button_re_0 = 0;

    int status_button_rc_1_star_stop = DEPRESSED_BUTTON;
    int count_button_rc_1_star_stop = 0;

    int status_button_rc_0_star_stop = DEPRESSED_BUTTON;
    int count_button_rc_0_star_stop = 0;

    steps_duration_beam = 1;
    steps_interval_beam = 1;
    //    int steps_d = 100, steps_i = 0;
    //    *steps_duration_beam = &steps_d;
    //    *steps_interval_beam = &steps_i;

    //    unsigned char index, counter = 0;
    init();
    while (1) {
        //        Set_segment_display_v_3(4,4);
        //        if (seconds == 0 || seconds == 1 || seconds == 2 || seconds == 3 || seconds == 4) {
        //            Set_segment_display_v_3(4, seconds);
        //        }
        //        if (seconds == 0 || seconds == 1 || seconds == 2 || seconds == 3 || seconds == 4) {

        //        Управление старта и запуска  (начало)

        if (PORTEbits.RE0 == PRESSED_BUTTON) {
            status_button_re_0 = PRESSED_BUTTON;
            //            count_button_re_0++;
        }


        if (status_button_re_0 == PRESSED_BUTTON) {
            count_button_re_0++;
        }

        if (status_button_re_0 == PRESSED_BUTTON && count_button_re_0 >= 10000) {
            count_button_re_0 = 0;
            status_button_re_0 = DEPRESSED_BUTTON;
            if (T1CONbits.TMR1ON == 1) {
                T1CONbits.TMR1ON = 0;
                T3CONbits.TMR3ON = 0;
                PORTBbits.RB1 = 1;
            } else {
                T1CONbits.TMR1ON = 1;
                T3CONbits.TMR3ON = 0;
            }
        }

        //        Управление старта и запуска  (конец)


        //        Установка больше длительности пучка  (начало)

        if (PORTEbits.RE2 == PRESSED_BUTTON) {
            status_button_rc_1_star_stop = PRESSED_BUTTON;
        }


        if (status_button_rc_1_star_stop == PRESSED_BUTTON) {
            count_button_rc_1_star_stop++;
        }

        if (status_button_rc_1_star_stop == PRESSED_BUTTON && count_button_rc_1_star_stop >= 1000) {
            count_button_rc_1_star_stop = 0;
            status_button_rc_1_star_stop = DEPRESSED_BUTTON;
            flag_view_steps_duration_beam = 0;
            flag_view_steps_interval_beam = 1;
            if (steps_interval_beam < 100) steps_interval_beam = steps_interval_beam + 1;
        }

        if (flag_view_steps_interval_beam) view_all_number_by_7_segments_by_4_elements(steps_interval_beam);
        
        

        //        Установка больше длительности пучка  (конец)
        
        //        Установка больше интервала между пучком  (начало)
        
        if (PORTEbits.RE1 == PRESSED_BUTTON) {
            status_button_rc_1_star_stop = PRESSED_BUTTON;
        }


        if (status_button_rc_1_star_stop == PRESSED_BUTTON) {
            count_button_rc_1_star_stop++;
        }

        if (status_button_rc_1_star_stop == PRESSED_BUTTON && count_button_rc_1_star_stop >= 1000) {
            count_button_rc_1_star_stop = 0;
            status_button_rc_1_star_stop = DEPRESSED_BUTTON;
            flag_view_steps_duration_beam = 1;
            flag_view_steps_interval_beam = 0;
            if (steps_duration_beam < 100) steps_duration_beam = steps_duration_beam + 1;
        }

        if (flag_view_steps_duration_beam) view_all_number_by_7_segments_by_4_elements(steps_duration_beam);
        
        //        Установка больше интервала между пучком  (конец)
        
        

        //                view_all_number_by_7_segments_by_4_elements(seconds);


    }

}

void switch_button_for_port_e(char bit_value) {
    do {
        while ((PORTE & bit_value) != bit_value);
        //        while((current_port & bit_value)!=bit_value) ClrWdt0;
        //        Delay1KTCYx(15);
    } while ((PORTE & bit_value) != bit_value);

    do { //ожидание нажатия 
        while ((PORTE & bit_value) == bit_value);
        //        while((current_port & bit_value)!=bit_value) ClrWdt0;
        //        Delay1KTCYx(15);
    } while ((PORTE & bit_value) == bit_value);
}

void switch_button(char current_port, char bit_value) {
    int delayTime = 15;
    do {
        int test;
        while ((current_port & bit_value) != bit_value);
        //        while((current_port & bit_value)!=bit_value) ClrWdt0;
        Delay1KTCYx(delayTime);
    } while ((current_port & bit_value) != bit_value);

    do { //ожидание нажатия 
        int test1;
        while ((current_port & bit_value) == bit_value);
        //        while((current_port & bit_value)!=bit_value) ClrWdt0;
        Delay1KTCYx(delayTime);
    } while ((current_port & bit_value) == bit_value);
}

void init(void) {
    CMCON = 0b00000111; // Close Comparator
    TRISA = 0b00010000; // RA4 is for Switch SW1 Input
    TRISB = 0b00000001; // RB0 is for Switch SW2 Input
    PORTB = 0b00001101;
    TRISC = 0b11111111;
    PORTCbits.RC0 = 1;
    PORTCbits.RC1 = 1;
    //    TRISC = 0b11111111;
    TRISD = 0b00000000;
    TRISE = 0b00000111; // RE3 is for Switch SW3 Input
    PORTEbits.RE0 = 1;
    PORTEbits.RE1 = 1;
    PORTEbits.RE2 = 1;
    ADCON1 = 0b00001111; // Configure Digital Channel
    RCONbits.IPEN = 1;
    TMR1L = 0x50;
    TMR1H = 0xc3;
    TMR3L = 0x50;
    TMR3H = 0xc3;
    //    T1CON = 0b11101111;
    T1CON = 0b10101101;
    T3CON = 0b10101100;
    seconds = minutes = hours = 0;
    IPR1bits.TMR1IP = 1;
    IPR2bits.TMR3IP = 1;
    PIE1bits.TMR1IE = 1;
    PIE2bits.TMR3IE = 1;
    INTCONbits.GIEH = 1;
}

void view_all_number_by_7_segments_by_4_elements(int current_number) {
    //    int first_number = -1;
    //    int second_number = -1;
    //    int third_number = -1;
    //    int fourth_number = -1;
    int b = 0;
    int i = 4;
    while (current_number > 0) {
        b = current_number % 10;
        b = current_number - (current_number / 10)*10;
        Set_segment_display_v_4(i, b);
        current_number /= 10;
        i--;
    }
}

void Set_segment_display_v_3(int Number_Led_Buffer, int Data_Byte) {
    switch (Number_Led_Buffer) {
        case 1:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 1;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
        case 2:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 1;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
        case 3:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 1;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
        case 4:
        {
            LEDbuf0 = 1;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
    }
    Delay100TCYx(1);
}

void Set_segment_display_v_4(int Number_Led_Buffer, int Data_Byte) {
    switch (Number_Led_Buffer) {
        case 1:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 1;
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
            //            Delay100TCYx(1);
            //            set_number(-1);
            break;
        }
        case 2:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 1;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
            //            Delay100TCYx(1);
            //            set_number(-1);
            break;
        }
        case 3:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 1;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
            //            Delay100TCYx(1);
            //            set_number(-1);
            break;
        }
        case 4:
        {
            LEDbuf0 = 1;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
            //            Delay100TCYx(1);
            //            set_number(-1);
            break;
        }
    }
    set_number(-1);
    //    Delay100TCYx(1);
}

void set_number(int Data_Number) {
    switch (Data_Number) {
        case 1:
        {
            LED0 = 0;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            break;
        }
        case 2:
        {

            LED0 = 1;
            LED1 = 1;
            LED2 = 0;
            LED3 = 1;
            LED4 = 1;
            LED5 = 0;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 3:
        {

            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 0;
            LED5 = 0;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 4:
        {
            LED0 = 0;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 5:
        {
            LED0 = 1;
            LED1 = 0;
            LED2 = 1;
            LED3 = 1;
            LED4 = 0;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 6:
        {
            LED0 = 1;
            LED1 = 0;
            LED2 = 1;
            LED3 = 1;
            LED4 = 1;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 7:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            break;
        }
        case 8:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 1;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 9:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 0;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 0:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 1;
            LED5 = 1;
            LED6 = 0;
            LED7 = 0;
            break;
        }
        case -1:
        {
            LED0 = 0;
            LED1 = 0;
            LED2 = 0;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            break;
        }
    }
}

