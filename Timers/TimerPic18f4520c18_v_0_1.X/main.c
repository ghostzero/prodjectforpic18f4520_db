
/*  
 ** Author: DAN1138 
 ** Date: 2013-FEB-16 
 ** File: main.c 
 ** Target: PIC18F2520 
 ** OS: Win7pro 64-bit 
 ** MPLAB: 8.87 
 ** Compiler: C18 v3.40 
 ** Application: TIMER1 as 32768Hz clock to generate a 1Hz interrupt 
 */
#include <p18f4520.h> 

#define LED0 PORTDbits.RD0
#define LED1 PORTDbits.RD1
#define LED2 PORTDbits.RD2
#define LED3 PORTDbits.RD3
#define LED4 PORTDbits.RD4
#define LED5 PORTDbits.RD5
#define LED6 PORTDbits.RD6
#define LED7 PORTDbits.RD7
#define LEDbuf0 PORTBbits.RB4
#define LEDbuf1 PORTBbits.RB5
#define LEDbuf2 PORTBbits.RB6
#define LEDbuf3 PORTBbits.RB7

/* Set up the configuration bits */
#pragma config OSC = HS 
#pragma config STVREN = ON 
#pragma config PWRT = OFF 
#pragma config BOREN = OFF 
#pragma config WDT = OFF 
#pragma config PBADEN = OFF 
#pragma config CCP2MX = PORTBE 
#pragma config MCLRE = ON 
#pragma config LVP = ON 
#pragma config FCMEN = OFF  
#pragma config IESO = OFF   
#pragma config BORV = 0     
#pragma config WDTPS = 32768 
#pragma config LPT1OSC = ON 
#pragma config CP0 = OFF  ,CP1 = OFF  ,CP2 = OFF  ,CP3 = OFF  ,CPB = OFF  ,CPD = OFF  
#pragma config WRT0 = OFF ,WRT1 = OFF ,WRT2 = OFF ,WRT3 = OFF ,WRTB = OFF ,WRTD = OFF 
#pragma config WRTC = OFF 
#pragma config EBTR0 = OFF,EBTR1 = OFF,EBTR2 = OFF,EBTR3 = OFF,EBTRB = OFF 

//#define FOSC 18432000L  /* this is the frequency that the CPU will run at */ 
#define FOSC 20000000L  /* this is the frequency that the CPU will run at */ 
#define FCYC FOSC/4L 


//  
// Global data 
//  
unsigned char gSecs;
unsigned char gMins;
unsigned char gHours;

//---------------------------------------------- 
// High priority interrupt vector 
//---------------------------------------------- 

void
InterruptHandlerHigh(
        void
        );

void
InterruptHandlerLow(
        void
        );


void view_all_number_by_7_segments_by_4_elements(int current_number);
void Set_segment_display_v_4(int Number_Led_Buffer, int Data_Byte);
void set_number(int Data_Number);

#pragma code InterruptVectorHigh = 0x08 

void
InterruptVectorHigh(
        void
        ) {
    _asm
            goto InterruptHandlerHigh
            _endasm //jump to interrupt routine 
}

//---------------------------------------------- 
// Low priority interrupt vector 
//---------------------------------------------- 
#pragma code InterruptVectorLow=0x18 

void
InterruptVectorLow(
        void
        ) {
    _asm goto InterruptHandlerLow _endasm
}

/* return to the default code section */
#pragma code 
//---------------------------------------------- 
// High priority interrupt handlers 
//---------------------------------------------- 

#pragma interrupt InterruptHandlerHigh 

void
InterruptHandlerHigh(
        void
        ) {
    if (PIE1bits.TMR1IE) {
        if (PIR1bits.TMR1IF) {
            PIR1bits.TMR1IF = 0;
            /* 
             ** We run TIMER1 in asynchronous mode so 
             ** speial care must be taken when changeing 
             ** the count values in TIMER1. 
             */
            while ((TMR1L & 1)); // wait for TIMER1 LSB to be 1 
            while (!(TMR1L & 1)); // wait for TIMER1 LSB to be 0 
            TMR1H |= 0x80; // Set TIMER1 to interrupt in 32768 counts 

            gSecs++;
            if (gSecs > 59) {
                gSecs = 0;
                gMins++;
                if (gMins > 59) {
                    gMins = 0;
                    gHours++;
                    if (gHours > 23) {
                        gHours = 0;
                    }
                }
            }
        }
    }
}

//---------------------------------------------- 
// Low priority interrupt handlers 
//---------------------------------------------- 
#pragma interruptlow InterruptHandlerLow 

void
InterruptHandlerLow(
        void
        ) {
}


//---------------------------------------------- 
// Initialize this PIC 
//---------------------------------------------- 

void
PIC_Init(
        void
        ) {
    // Keep us happy by turning off the comparators 
    // and letting their pins be digital I/O. 
    CMCON = 0x07; // Turn off comparators 

    // Keep simulator happy by initializing the ADC 
    // Keep us happy by turning the ADC off. 
    ADCON2 = 0x02; // Select internal RC as ADC clock source 
    ADCON1 = 0x0F; // set all ADC inputs for digital I/O 
    ADCON0 = 0x00; // Turn off ADC 

    // initialize my interrupt handlers 

    /* Place code here to initialize interrupt handlers */

    // 
    // Initialize TIMER1 as 327678 real time clock 
    //    
    PIE1bits.TMR1IE = 0; //turn off the interrupt for timer1 
    T1CON = 0b00001110; //8-bit mode, no prescale, 32.768kHz oscillator, asynchronous mode 
    TMR1H = 0x80;
    TMR1L = 0x00; //set TIMER1 to interrupt in 32768 counts 
    IPR1bits.TMR1IP = 1; //use high priority interrupt vector 
    PIR1bits.TMR1IF = 0;
    PIE1bits.TMR1IE = 1; //turn on the interrupt for TIMER1 
    T1CON = T1CON | 1; //turn on TIMER1 

    // Enable the interrupt system    
    RCONbits.IPEN = 1; // Enable High/Low interrupt priority model.    
    // Note: High priority interrupts must be enabled for 
    //       low priority interrupts to happen. 
    INTCONbits.GIEL = 1; // Enable low priority interrupt sources 
    INTCONbits.GIEH = 1; // Enable high priority interrupt sources 

}

void
main(
        void
        ) {
    // Set time of day 
    gSecs = 0;
    gMins = 0;
    gHours = 12;

    PIC_Init();

    //    for( ; ; ); // loop forever 
    while (1) {

        view_all_number_by_7_segments_by_4_elements(gSecs);
    }
}

void view_all_number_by_7_segments_by_4_elements(int current_number) {
    //    int first_number = -1;
    //    int second_number = -1;
    //    int third_number = -1;
    //    int fourth_number = -1;
    int b = 0;
    int i = 4;
    while (current_number > 0) {
        b = current_number % 10;
        b = current_number - (current_number / 10)*10;
        Set_segment_display_v_4(i, b);
        current_number /= 10;
        i--;
    }
}

void Set_segment_display_v_4(int Number_Led_Buffer, int Data_Byte) {
    switch (Number_Led_Buffer) {
        case 1:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 1;
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
            //            Delay100TCYx(1);
            //            set_number(-1);
            break;
        }
        case 2:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 1;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
            //            Delay100TCYx(1);
            //            set_number(-1);
            break;
        }
        case 3:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 1;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
            //            Delay100TCYx(1);
            //            set_number(-1);
            break;
        }
        case 4:
        {
            LEDbuf0 = 1;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
            //            Delay100TCYx(1);
            //            set_number(-1);
            break;
        }
    }
    set_number(-1);
    //    Delay100TCYx(1);
}

void set_number(int Data_Number) {
    switch (Data_Number) {
        case 1:
        {
            LED0 = 0;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            break;
        }
        case 2:
        {

            LED0 = 1;
            LED1 = 1;
            LED2 = 0;
            LED3 = 1;
            LED4 = 1;
            LED5 = 0;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 3:
        {

            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 0;
            LED5 = 0;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 4:
        {
            LED0 = 0;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 5:
        {
            LED0 = 1;
            LED1 = 0;
            LED2 = 1;
            LED3 = 1;
            LED4 = 0;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 6:
        {
            LED0 = 1;
            LED1 = 0;
            LED2 = 1;
            LED3 = 1;
            LED4 = 1;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 7:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            break;
        }
        case 8:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 1;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 9:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 0;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 0:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 1;
            LED5 = 1;
            LED6 = 0;
            LED7 = 0;
            break;
        }
        case -1:
        {
            LED0 = 0;
            LED1 = 0;
            LED2 = 0;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            break;
        }
    }
}