/*
 * File:   main.c
 * Author: Ghost
 *
 * Created on 6 августа 2016 г., 20:10
 */


#include "configure.h"
#include <delays.h>


#define LED0 PORTDbits.RD0
#define LED1 PORTDbits.RD1
#define LED2 PORTDbits.RD2
#define LED3 PORTDbits.RD3
#define LED4 PORTDbits.RD4
#define LED5 PORTDbits.RD5
#define LED6 PORTDbits.RD6
#define LED7 PORTDbits.RD7
#define LEDbuf0 PORTBbits.RB4
#define LEDbuf1 PORTBbits.RB5
#define LEDbuf2 PORTBbits.RB6
#define LEDbuf3 PORTBbits.RB7
#define STAR 1;
#define STOP -1;

int PRESSED_BUTTON = 0, DEPRESSED_BUTTON = 1;
int k = 4, i = 1, time_step = 1;
int flag_start_stop;
int milliseconds;



void init(void);
void Set_segment_display_v_3(int Number_Led_Buffer, int Data_Byte);
void Set_segment_display_v_4(int Number_Led_Buffer, int Data_Byte);
void set_number(int Data_Number);
void MyHighInt();

#pragma interrupt MyHighInt
#pragma code high_vector = 0x08

/************ вектор высокоприоритетного прерывания ***************/

void high_vector(void) {
    _asm GOTO MyHighInt _endasm;
}

void main(void) {

    while (1) {

    }
    return;
}

//Процедура обслуживания прерывания

void MyHighInt(void) {

//    if (PIR1bits.TMR1IF == 1) {
//        TMR1L = 0x50;
//        TMR1H = 0xC3;
//        PIR1bits.TMR1IF = 0;
//        if (time_step == 100) {
//            time_step = 0;
//            //            DoTime();
//        }
//        time_step++;
//    }
    if (INTCONbits.INT0IF == 1) {
        int test = 1;

    }

}

void init(void) {
    CMCON = 0b00000111; // Close Comparator

    TRISA = 0b00010000; // RA4 is for Switch SW1 Input
    TRISB = 0b00000001; // RB0 is for Switch SW2 Input
    PORTB = 0b00001111;
    TRISC = 0b00000011;
    //    TRISC = 0b11111111;
    TRISD = 0b00000000;
    TRISE = 0b00000111; // RE3 is for Switch SW3 Input
    //    ADCON1=0x0F;
    ADCON1 = 0b00001111; // Configure Digital Channel
    RCONbits.IPEN = 1;
    TMR1L = 0x50;
    TMR1H = 0xC3;
    //    T1CON = 0b11101111;
    T1CON = 0b10101101;
    //  turn off timer T1CON
    //  T1CON = 0b00000000;

    milliseconds = 0;
    IPR1bits.TMR1IP = 1;
    PIE1bits.TMR1IE = 1;
    INTCONbits.GIEH = 1;
    INTCONbits.INT0IE = 1;
    INTCONbits.INT0IF = 0;
    INTCONbits.GIE = 0;
}

void Set_segment_display_v_3(int Number_Led_Buffer, int Data_Byte) {
    switch (Number_Led_Buffer) {
        case 1:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 1;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
        case 2:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 1;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
        case 3:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 1;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
        case 4:
        {
            LEDbuf0 = 1;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
    }
    Delay100TCYx(1);
}

void Set_segment_display_v_4(int Number_Led_Buffer, int Data_Byte) {
    switch (Number_Led_Buffer) {
        case 1:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 1;
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
            //            Delay100TCYx(1);
            //            set_number(-1);
            break;
        }
        case 2:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 1;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
            //            Delay100TCYx(1);
            //            set_number(-1);
            break;
        }
        case 3:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 1;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
            //            Delay100TCYx(1);
            //            set_number(-1);
            break;
        }
        case 4:
        {
            LEDbuf0 = 1;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
            //            Delay100TCYx(1);
            //            set_number(-1);
            break;
        }
    }
    set_number(-1);
    //    Delay100TCYx(1);
}

void set_number(int Data_Number) {
    switch (Data_Number) {
        case 1:
        {
            LED0 = 0;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            break;
        }
        case 2:
        {

            LED0 = 1;
            LED1 = 1;
            LED2 = 0;
            LED3 = 1;
            LED4 = 1;
            LED5 = 0;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 3:
        {

            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 0;
            LED5 = 0;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 4:
        {
            LED0 = 0;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 5:
        {
            LED0 = 1;
            LED1 = 0;
            LED2 = 1;
            LED3 = 1;
            LED4 = 0;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 6:
        {
            LED0 = 1;
            LED1 = 0;
            LED2 = 1;
            LED3 = 1;
            LED4 = 1;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 7:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            break;
        }
        case 8:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 1;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 9:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 0;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 0:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 1;
            LED5 = 1;
            LED6 = 0;
            LED7 = 0;
            break;
        }
        case -1:
        {
            LED0 = 0;
            LED1 = 0;
            LED2 = 0;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            break;
        }
    }
}


