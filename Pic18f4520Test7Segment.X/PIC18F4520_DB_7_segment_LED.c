/*
Этот пример показывает как выводить информацию на обычный 7-сегментный дисплей с помощью 
микроконтроллера PIC18F4520
PORTD контроллера подключен к выводам 7-сегментного дисплея, PORTB контроллера подключен к
микросхеме ULN2003, которая управляет катодами светодиодного индикатора
Для работы светодиодного дисплея необходимо снять LCD дисплей с платы
*/
#include <p18f4520.h>
#include <delays.h>
#pragma config OSC = HSPLL
#pragma config PWRT = OFF
#pragma config BOREN = OFF
#pragma config WDT = OFF
#pragma config MCLRE = ON
#pragma config PBADEN = OFF
#pragma config LVP = OFF
#define LED0 PORTDbits.RD0
#define LED1 PORTDbits.RD1
#define LED2 PORTDbits.RD2
#define LED3 PORTDbits.RD3
#define LED4 PORTDbits.RD4
#define LED5 PORTDbits.RD5
#define LED6 PORTDbits.RD6
#define LED7 PORTDbits.RD7
#define LEDbuf0 PORTBbits.RB4
#define LEDbuf1 PORTBbits.RB5
#define LEDbuf2 PORTBbits.RB6
#define LEDbuf3 PORTBbits.RB7
// Initializtion
void init(void)
{
CMCON=0b00000111; // Close Comparator
TRISA=0b00010000; // RA4 is for Switch SW1 Input
TRISB=0b00000001; // RB0 is for Switch SW2 Input
TRISC=0b00000000;
TRISD=0b00000000;
TRISE=0b00001000; // RE3 is for Switch SW3 Input
ADCON1=0b00001111; // Configure Digital Channel
}
// Main Programmer
void main( void )
{
init();
while(1)
{
LEDbuf0=0;
LEDbuf1=0;
LEDbuf2=0;
LEDbuf3=1;
LED0=0;
LED1=1;
LED2=1;
LED3=0;
LED4=0;
LED5=0;
LED6=0;
LED7=0;
Delay100TCYx(2);
LEDbuf0=0;
LEDbuf1=0;
LEDbuf2=1;
LEDbuf3=0;
LED0=1;
LED1=1;
LED2=0;
LED3=1;
LED4=1;
LED5=0;
LED6=1;
LED7=0;
Delay100TCYx(2);
LEDbuf0=0;
LEDbuf1=1;
LEDbuf2=0;
LEDbuf3=0;
LED0=1;
LED1=1;
LED2=1;
LED3=1;
LED4=0;
LED5=0;
LED6=1;
LED7=0;
Delay100TCYx(2);
LEDbuf0=1;
LEDbuf1=0;
LEDbuf2=0;
LEDbuf3=0;
LED0=0;
LED1=1;
LED2=1;
LED3=0;
LED4=0;
LED5=1;
LED6=1;
LED7=0;
Delay100TCYx(2);
}
}