/*
 * File:   main.c
 * Author: Ghost
 *
 * Created on 4 августа 2016 г., 8:26
 */
//Пример 6.18

/* Установить биты конфигурации 
 * - Установить генератор
 * - Отключить сторожевой таймер
 * - Отключить низковальтное программирование
 * - Отключить сброс по частичной потере питания
 * - Разрешить общий сброс
 *
 *
 */

//#include <p18f4520.h>
//#include <p18f4520.h>

#include "configure.h"
#include <delays.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
#define LED0 PORTDbits.RD0
#define LED1 PORTDbits.RD1
#define LED2 PORTDbits.RD2
#define LED3 PORTDbits.RD3
#define LED4 PORTDbits.RD4
#define LED5 PORTDbits.RD5
#define LED6 PORTDbits.RD6
#define LED7 PORTDbits.RD7
#define LEDbuf0 PORTBbits.RB4
#define LEDbuf1 PORTBbits.RB5
#define LEDbuf2 PORTBbits.RB6
#define LEDbuf3 PORTBbits.RB7


int k = 4, i = 1;
int PRESSED_BUTTON = 0, DEPRESSED_BUTTON = 1;
int count_timer = 0;



/************прототипы функций обработки прерываний ***************/
void init(void);
void Set_segment_display_v_3(int Number_Led_Buffer, int Data_Byte);
void set_number(int Data_Number);
void MyHighInt();
void DoTime();
void view_all_number_by_7_segments_by_4_elements(int current_number);

#pragma interrupt MyHighInt
#pragma code high_vector = 0x08

/************ вектор высокоприоритетного прерывания ***************/

void high_vector(void) {
    _asm GOTO MyHighInt _endasm;
}

/************ память данных ***************/

// счетчики времени дня

#pragma udata access IntMyData  // размещение в банке доступа

near char seconds;
near char minutes;
near char hours;

// ************************** функций ***********************************

#pragma code

//функций инкрементирования таймера

void DoTime() { //формирование сигнала счета каждую секунду
    //    TMR1H = 0x8000;
    //    TMR1L = 0xFF;
    //    TMR1H = 0x01;
    //    TMR1L = 0xF0;

//    TMR1L = 0x88;
//    TMR1H = 0x13;
//    PIR1bits.TMR1IF = 0;

    seconds++;
    if (seconds == 60) {
        seconds = 0;
        minutes++;
        if (minutes == 60) {
            minutes = 0;
            hours++;
            if (hours == 24) {
                hours = 0;
            }
        }

    }

}

//Процедура обслуживания прерывания

void MyHighInt(void) {
    if (PIR1bits.TMR1IF == 1) {
        TMR1L = 0x88;
        TMR1H = 0x13;
        PIR1bits.TMR1IF = 0;
        if (count_timer == 1000)
            DoTime();
        count_timer++;
    }
}

void main(void) {
    init();
    while (1) {
        //        if (seconds == 0 || seconds == 1 || seconds == 2 || seconds == 3 || seconds == 4) {
        //            Set_segment_display_v_3(4, seconds);
        //        }
        //        if (seconds == 0 || seconds == 1 || seconds == 2 || seconds == 3 || seconds == 4) {
        view_all_number_by_7_segments_by_4_elements(seconds);
        //        }
    }
    //    return;
}

void init(void) {
    CMCON = 0b00000111; // Close Comparator
    TRISA = 0b00010000; // RA4 is for Switch SW1 Input
    TRISB = 0b00000001; // RB0 is for Switch SW2 Input
    PORTB = 0b00001111;
    TRISC = 0b00000000;
    //    TRISC = 0b11111111;
    TRISD = 0b00000000;
    TRISE = 0b00001111; // RE3 is for Switch SW3 Input
    ADCON1 = 0b00001111; // Configure Digital Channel
    RCONbits.IPEN = 1;
    TMR1L = 0x88;
    TMR1H = 0x13;
    //    TMR1L = 0xFF;
    //    TMR1H = 0xFF;
    T1CON = 0b10001101;
    seconds = minutes = hours = 0;
    IPR1bits.TMR1IP = 1;
    PIE1bits.TMR1IE = 1;
    INTCONbits.GIEH = 1;
}

void view_all_number_by_7_segments_by_4_elements(int current_number) {
    //    int first_number = -1;
    //    int second_number = -1;
    //    int third_number = -1;
    //    int fourth_number = -1;
    int b = 0;
    int i = 4;
    while (current_number > 0) {
        b = current_number % 10;
        b = current_number - (current_number / 10)*10;
        Set_segment_display_v_3(i, b);
        current_number /= 10;
        i--;
    }
}

void Set_segment_display_v_3(int Number_Led_Buffer, int Data_Byte) {
    switch (Number_Led_Buffer) {
        case 1:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 1;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
        case 2:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 1;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
        case 3:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 1;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
        case 4:
        {
            LEDbuf0 = 1;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
    }
    Delay100TCYx(1);
}

void set_number(int Data_Number) {
    switch (Data_Number) {
        case 1:
        {
            LED0 = 0;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            break;
        }
        case 2:
        {

            LED0 = 1;
            LED1 = 1;
            LED2 = 0;
            LED3 = 1;
            LED4 = 1;
            LED5 = 0;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 3:
        {

            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 0;
            LED5 = 0;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 4:
        {
            LED0 = 0;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 5:
        {
            LED0 = 1;
            LED1 = 0;
            LED2 = 1;
            LED3 = 1;
            LED4 = 0;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 6:
        {
            LED0 = 1;
            LED1 = 0;
            LED2 = 1;
            LED3 = 1;
            LED4 = 1;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 7:
        {
            LED0 = 1;
            LED1 = 0;
            LED2 = 0;
            LED3 = 0;
            LED4 = 0;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 8:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 1;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 9:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 1;
            LED5 = 0;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 0:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 1;
            LED5 = 1;
            LED6 = 0;
            LED7 = 0;
            break;
        }
        case -1:
        {
            LED0 = 0;
            LED1 = 0;
            LED2 = 0;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            break;
        }
    }
}