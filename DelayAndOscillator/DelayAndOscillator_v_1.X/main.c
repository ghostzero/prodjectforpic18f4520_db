/*
 * File:   main.c
 * Author: Ghost
 *
 * Created on 6 августа 2016 г., 15:32
 */


#include "configure.h"
#include <delays.h>
void delaysecond_user(int number_seconds);
void init(void);

void main(void) {
    init();
    while (1) {
        PORTB = 0b00000001;
        delaysecond_user(10);
        PORTB = 0b00001111;
        delaysecond_user(2);
}
    return;
}

void delaysecond_user(int number_seconds) {
    int i;
    for (i = 0; i < number_seconds; i++) {
        Delay10KTCYx(25);
    }
}

void init(void) {
    CMCON = 0b00000111; // Close Comparator
    TRISA = 0b00010000; // RA4 is for Switch SW1 Input
    TRISB = 0b00000001; // RB0 is for Switch SW2 Input
    PORTB = 0b00001111;
    TRISC = 0b00000000;
    //    TRISC = 0b11111111;
    TRISD = 0b00000000;
    TRISE = 0b00001111; // RE3 is for Switch SW3 Input
    ADCON1 = 0b00001111; // Configure Digital Channel
}
