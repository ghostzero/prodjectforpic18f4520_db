/*
 * File:   InterrupPic4520Test1.c
 * Author: Ghost
 *
 * Created on 19 июня 2016 г., 11:25
 */


#include <xc.h>
#include "config.h"

#define _XTAL_FREQ 40000000 //What ever speed you set your internal(or)external oscillator

int i = 0;
void DelayHalfSecond(void);
void SetupClock(void);
int flag_view = 1;
int PRESSED_BUTTON = 0, DEPRESSED_BUTTON = 1;
int LOW = 0, HIGH = 1;
void delay_ms(unsigned long x);

void main(int argc, char** argv) {

//    SetupClock();
     TRISA = 0b00010000; // RA4 is for Switch SW1 Input
    TRISB = 0b00000001; // RB0 is for Switch SW2 Input
    //    PORTB = 0b00001110;
    PORTB = 0b00001010;
    TRISBbits.RB4 = 0; //set RB4 as Output
    TRISBbits.RB0 = 1; //set RB0 as Input
    
    INTCONbits.INT0IE = 1;
//    INTCONbits.INT0E = 1; //enable Interrupt 0 (RB0 as interrupt)
//    INTCON2bits.INTEDG0 = 0; //cause interrupt at falling edge
//    INTCONbits.INT0F = 0; //reset interrupt flag
    INTCONbits.INT0IF = 0;
     
    
    
    
    

   
    ei();     // This is like fliping the master switch to enable interrupt

    while(1) //infinite loop
    {
      //actually we have to put the processor in sleep which i will cover
      //  in later tutorials
        if (flag_view == 1) PORTB = 0b00001101;
        if (flag_view == 2) PORTB = 0b00001011;
    }


}

void delay_ms(unsigned long x)
{
    for(i=0;i<x;i++)
        __delay_ms(1);
}


void DelayHalfSecond()
{
    for(i=0;i<50;i++)
        __delay_ms(10);
}

void SetupClock()
{
    OSCCONbits.IRCF0 = 1;
    OSCCONbits.IRCF1 = 1;
    OSCCONbits.IRCF2 = 1;
}

//void interrupt low_priority CheckButtonPressed()
//{
//    //check if the interrupt is caused by the pin RB0
//    if(INTCONbits.INT0F == 1)
//    {
//        if (flag_view == 1) flag_view=2; else flag_view=1;
////        LATB4 = !LATB4;
//        delay_ms(200);
////        DelayHalfSecond(); //like a debounce
//        INTCONbits.INT0F = 0;
//    }
//
//}

void interrupt high_priority CheckButtonPressed()
{
    //check if the interrupt is caused by the pin RB0
    if(INTCONbits.INT0IF == 1)
    {
        if (flag_view == 1) flag_view=2; else flag_view=1;
//        LATB4 = !LATB4;
        delay_ms(200);
//        DelayHalfSecond(); //like a debounce
        INTCONbits.INT0IF = 0;
    }

}
