
#define _XTAL_FREQ 40000000 //The speed of your internal(or)external oscillator
#define USE_AND_MASKS

#include <xc.h>
#include "config.h"
#include <plib/timers.h>
#include <plib/pwm.h>

int i = 0;
int countup = 1; //our flag to set up/down dutycycle
int DutyCycle = 0;
unsigned char Timer2Config; //PWM clock source
void SetupClock(void);

void main(int argc, char** argv) {

    //    I use external clocl frequency 10MHz
    SetupClock(); // Internal Clock to 8MHz

    TRISCbits.TRISC2 = 0; //PWM pin set as output

    Timer2Config = T2_PS_1_16; //prescale 16
    OpenTimer2(Timer2Config);

    OpenPWM1(0x3E); //Open pwm at 10KHz

    while(1) //infinite loop
    {
        if(countup == 1)
        {
            SetDCPWM1(DutyCycle); //Change duty cycle
            DutyCycle++;
            if(DutyCycle == 1024)
                countup = 0;
        }
        else
        {
            SetDCPWM1(DutyCycle); //Change duty cycle
            DutyCycle--;
            if(DutyCycle == 0)
                countup = 1;
        }
        __delay_ms(1);
    }
}

void SetupClock()
{
//    I use external clocl frequency 10MHz
//    OSCCONbits.IRCF0 = 1;
//    OSCCONbits.IRCF1 = 1;
//    OSCCONbits.IRCF2 = 1;
}